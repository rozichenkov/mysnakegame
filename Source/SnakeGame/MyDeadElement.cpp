// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDeadElement.h"
#include "SnakeBase.h"

// Sets default values
AMyDeadElement::AMyDeadElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	

}

// Called when the game starts or when spawned
void AMyDeadElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyDeadElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyDeadElement::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{

		auto snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(snake))
		{

			snake->Destroy();







		}


	}
}